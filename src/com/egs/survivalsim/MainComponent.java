package com.egs.survivalsim;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;

import javax.swing.JFrame;

import com.egs.survivalsim.game.GameWindow;
import com.egs.survivalsim.gui.Menu;
import com.egs.survivalsim.util.FileHandler;
import com.egs.survivalsim.util.InputHandler;

public class MainComponent extends Canvas implements Runnable{

	private static final long serialVersionUID = 1L;
	
	private static final int WIDTH = 720;
	private static final int HEIGHT = 410;
	private static final String VERSION = "v0.6.4a";
	private static final String NAME = "Survival Sim " + VERSION;
	private boolean running = false;
	
	private FileHandler file;
	private InputHandler input;
	private Menu menu;
	private Graphics g;
	private GameWindow gameWindow;
	
	private int fps;
	private int ups;
	
	private int state = 0;
	private int startSize = 0;
	private int startTime = 0;
	private int structSize = 0;
	private String structName = null;
	private int type = 0;
	private boolean started = false;

	public MainComponent(){
		input = new InputHandler(this);
		menu = new Menu(this, input);
		file = new FileHandler();
		file.createMainFolder();
	}
	
	public static void main(String[] args){
		MainComponent main = new MainComponent();
		Dimension size = new Dimension(WIDTH, HEIGHT);
		main.setPreferredSize(size);
		main.setMinimumSize(size);
		main.setMaximumSize(size);
		
		JFrame frame = new JFrame(NAME);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.add(main);
		frame.pack();
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		
		main.requestFocusInWindow();
		main.start();
	}
	
	public synchronized void start(){
		running = true;
		new Thread(this).start();
	}

	public synchronized void stop(){
		running = false;
	}
	
	public void run() {
		
		long lastTime = System.nanoTime();
		double unprocessed = 0;
		double ns = 1000000000.0 / 60.0;
		int frames = 0;
		int updates = 0;
		long last = System.currentTimeMillis();
		while(running){
			long now = System.nanoTime();
			unprocessed += (now - lastTime) / ns;
			lastTime = now;
			boolean render = true;
			while(unprocessed >= 1){
				updates++;
				update();
				unprocessed -= 1;
				render  = true;
			}
			
			if(render){
				frames++;
				render();
			}
			
			if(System.currentTimeMillis() - last > 1000){
				last += 1000;
				System.out.println(updates + " ups, " + frames + " fps");
				fps = frames;
				ups = updates;
				frames = 0;
				updates = 0;
			}
		}
		stop();
	}
	
	public void render(){
		BufferStrategy bs = getBufferStrategy();
		if(bs == null){
			createBufferStrategy(3);
			return;
		}
		
		g = bs.getDrawGraphics();
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, getWidth(), getHeight());
		
		if(state == 0) menu.render(g); //Render menus
		if(state == 1 && started) gameWindow.render(g); //Render game
		
		g.setColor(Color.WHITE);
		g.setFont(new Font("Arial", Font.PLAIN, 9));
		g.drawString(VERSION, 689, 9);
		
		g.dispose();
		bs.show();
	}
	
	public void update(){
		if(state == 1 && !started){
			gameWindow = new GameWindow(this, input, g, startSize, startTime, type, structSize, structName);
			started = true;
		}
		
		if(state == 0) menu.update(); //Update Menus
		if(state == 1 && started) gameWindow.update(); //Update game
		input();
		input.release();
	}
	
	public void input(){
		if(input.esc.isPressed()){		
			reset();
		}
	}
	
	
	public void reset(){
		setState(0);
		started = false;
		gameWindow = null;
		menu.resetMenus();
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public int getUps() {
		return ups;
	}

	public void setUps(int ups) {
		this.ups = ups;
	}

	public int getFps() {
		return fps;
	}

	public void setFps(int fps) {
		this.fps = fps;
	}

	public int getStartSize() {
		return startSize;
	}

	public void setStartSize(int startSize) {
		this.startSize = startSize;
	}

	public int getStartTime() {
		return startTime;
	}

	public void setStartTime(int startTime) {
		this.startTime = startTime;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getStructSize() {
		return structSize;
	}

	public void setStructSize(int structSize) {
		this.structSize = structSize;
	}

	public String getStructName() {
		return structName;
	}

	public void setStructName(String structName) {
		this.structName = structName;
	}
	
}
