package com.egs.survivalsim.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileHandler {

	private String location = System.getenv("APPDATA") + "/.survivalsim";
	private File dir;
	private File file;
	private File chunk;
	private int worlds;
	
	public FileHandler(){
		createMainFolder();
	}
	
	public int getWorlds(){
		return worlds;
	}
	
	public void createMainFolder(){
		dir = new File(location);
		dir.mkdirs();
		worlds = dir.listFiles().length + 1;
	}
	
	public boolean checkWorlds(){
		System.out.println("Checking worlds");
		if(worlds <= 5) return true;
		else return false;
	}
	
	public void makeWorldFolder(){
		System.out.println("Making world folder");
		if(worlds > 5) return;
		dir = new File(location + "/world_" + worlds);
		dir.mkdirs();
	}
	
	public void newChunkFolder(int world){
		dir = new File(location + "/world_" + world + "/chunkdata");
		dir.mkdirs();
	}
	
	public void newHeightFolder(int world){
		dir = new File(location + "/world_" + world + "/chunkdata/a");
		dir.mkdirs();
	}
	
	public void saveChunk(byte[] result, int world){
		System.out.print("SAVING CHUNK");
		int chunks = 0;
		file = new File(location + "/world_" + (world - 1) + "/chunkdata/a/");
		System.out.print("Chunks: " + chunks);
		chunks = file.listFiles().length + 1;
		chunk = new File(location + "/world_" + (world - 1) + "/chunkdata/a/" + chunks + "a.dat");
		
		try {
			FileOutputStream out = new FileOutputStream(chunk);
			if(!chunk.exists()) chunk.createNewFile();
			out.write(result);
			out.flush();
			out.close();
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
	}
	
//	public void saveChunk(byte[][] data){
//		if(dirs <= 5){
//			makeWorldFolder();
//		}
//		
//		
//		
//		
//		
//	}
//
//	public void makeDir(){
//		file = new File(location);
//		file.mkdirs();
//	}
//	
//	public void makeWorldFolder(){
//		dirs = file.listFiles().length + 1;
//		if(dirs == 5) return;
//		file = new File(location +"/world_" + dirs);
//		file.mkdirs();
//	}
}
