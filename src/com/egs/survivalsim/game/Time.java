package com.egs.survivalsim.game;

public class Time {

	private long oldSteps = 0L;
	
	private int minute;
	private int hour;
	private int day;
	private int month;
	private int year;
	private int season;
	
	private String time = null;
	private String date = null;
	
	public Time(){
		System.out.println("NEW TIME");
	}
	
	public void update(long steps){
		if(steps >= oldSteps + 12){
			oldSteps = steps;
			if(minute < 59){
				minute++;
			}else{
				minute = 0;
				if(hour < 23){
					hour++;
				}else{
					hour = 0;
					if(day < 30){
						day++;
					}else{
						day = 1;
						if(month < 12){
							month++;
						}else{
							month = 1;
							year++;
						}
					}
				}
			}
		}
		
		if(day >= 20 && month >= 3 && month < 6){
			season = 1;
		}else if(day >= 21 && month >= 6 && month < 9){
			season = 2;
		}else if(day >= 22 && month >= 9 && month < 12){
			season = 3;
		}else if(day >= 21 && month >= 12){
			season = 0;
		}
		
		time = Integer.toString(hour) + ":" + Integer.toString(minute);
		date = Integer.toString(day) + "/" + Integer.toString(month) + "/" + Integer.toString(year);
		
	}

	public int getMinute() {
		return minute;
	}

	public void setMinute(int minute) {
		this.minute = minute;
	}

	public int getHour() {
		return hour;
	}

	public void setHour(int hour) {
		this.hour = hour;
	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getSeason() {
		return season;
	}

	public void setSeason(int season) {
		this.season = season;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}
}
