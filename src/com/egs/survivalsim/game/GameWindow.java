package com.egs.survivalsim.game;

import java.awt.Graphics;

import com.egs.survivalsim.MainComponent;
import com.egs.survivalsim.game.level.LevelAdventure;
import com.egs.survivalsim.game.level.LevelSimulation;
import com.egs.survivalsim.game.level.LevelStructure;
import com.egs.survivalsim.gui.GameInterface;
import com.egs.survivalsim.util.InputHandler;

public class GameWindow {
	
	private InputHandler input;
	private GameInterface gameInterface;
	private LevelAdventure levelAdventure;
	private LevelSimulation levelSimulation;
	private LevelStructure levelStructure;
	private Time time;
	private MainComponent main;
	
	private int startSize;
	private int startTime;
	private int structSize;
	private String structName;
	private int type;
	
	private double uTime;
	private double rTime;
	private String updateTime;
	private String renderTime;
	
	private long steps = 0L;
	private long oldStep = 0L;
	
	public GameWindow(MainComponent main, InputHandler input, Graphics g, int startSize, int startTime, int type, int structSize, String structName){
		this.input = input;
		this.startSize = startSize;
		this.startTime = startTime;
		this.structName = structName;
		this.type = type;
		this.main = main;
		time = new Time();
		gameInterface = new GameInterface(main, this, time, type);
		init();
	}
	
	public void init(){
		if(type == 0){
			if(startSize == 0) levelSimulation = new LevelSimulation(128, 128, input, gameInterface, time, main);
			if(startSize == 1) levelSimulation = new LevelSimulation(256, 256, input, gameInterface, time, main);
			if(startSize == 2) levelSimulation = new LevelSimulation(512, 512, input, gameInterface, time, main);
			if(startSize == 3) levelSimulation = new LevelSimulation(1024, 1024, input, gameInterface, time, main);
			if(startSize == 4) levelSimulation = new LevelSimulation(2048, 2048, input, gameInterface, time, main);
		}
		
		if(type == 1){ // 4096 8192
			levelAdventure = new LevelAdventure(512, 512, input, gameInterface, time, main);
		}
		
		if(type == 2){
			levelStructure = new LevelStructure(64, 64, structSize, structName, input, gameInterface, time, main);
		}
		
		if(startTime == 0){
			time.setDay(20);
			time.setMonth(3);
			time.setSeason(1);
		}
		if(startTime == 1){
			time.setDay(21);
			time.setMonth(6);
			time.setSeason(2);
		}
		if(startTime == 2){
			time.setDay(22);
			time.setMonth(9);
			time.setSeason(3);
		}
		if(startTime == 3){
			time.setDay(21);
			time.setMonth(12);
			time.setSeason(0);
		}
	}
	
	public void render(Graphics g){
		double rStart = System.nanoTime();
		if(type == 0) levelSimulation.render(g);
		if(type == 1) levelAdventure.render(g);
		if(type == 2) levelStructure.render(g);
		gameInterface.render(g);
		rTime = (System.nanoTime() - rStart) / 1000000;
	}
	
	public void update(){
		double uStart = System.nanoTime();
		if(type == 0) levelSimulation.update();
		if(type == 1) levelAdventure.update();
		if(type == 2) levelStructure.update();
		gameInterface.update(steps);
		updateTimes();
		steps++;
		uTime = (System.nanoTime() - uStart) / 1000000;
	}
	
	public void updateTimes(){
		if(steps >= oldStep + 10){
			oldStep = steps;
						
			if(uTime <= 0.001000) uTime = 0.001000;
			if(uTime >= 9.999999) uTime = (int) uTime;
						
			if(rTime <= 0.001000) rTime = 0.001000;
			if(rTime >= 9.999999) rTime = (int) rTime;
			
			String uString = Double.toString(uTime);
			String rString = Double.toString(rTime);
			String uSub = "0.000";
			String rSub = "0.000";
			
			if(uString.length() >= 5)uSub = uString.substring(0, 5);
			else uSub = uString.substring(0, uString.length());
			
			if(rString.length() >= 5) rSub = rString.substring(0, 5);
			else rSub = rString.substring(0, rString.length());
			
			updateTime = uSub;
			renderTime = rSub;
		}
	}

	public String getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}

	public String getRenderTime() {
		return renderTime;
	}

	public void setRenderTime(String renderTime) {
		this.renderTime = renderTime;
	}

	public long getSteps() {
		return steps;
	}

	public void setSteps(long steps) {
		this.steps = steps;
	}
}
