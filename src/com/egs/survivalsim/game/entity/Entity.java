package com.egs.survivalsim.game.entity;

import com.egs.survivalsim.game.level.Level;
import com.egs.survivalsim.util.InputHandler;

public class Entity {

	protected InputHandler input;
	protected Level level;
	
	protected int id;
	protected int type;
	protected int x, y;
	
	public Entity(int id, int type, int x, int y, InputHandler input, Level level){
		this.id = id;
		this.type = type;
		this.x = x;
		this.y = y;
		this.input = input; 
		this.level = level;
	}
	
	public void update(){
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}
}
