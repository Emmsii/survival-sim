package com.egs.survivalsim.game.entity.mob;

import com.egs.survivalsim.game.entity.Entity;
import com.egs.survivalsim.game.level.Level;
import com.egs.survivalsim.util.InputHandler;

public class Mob extends Entity{

	private int health;
	
	public Mob(int id, int type, int x, int y, InputHandler input, Level level) {
		super(id, type, x, y, input, level);
	}

	public int getHealth() {
		return health;
	}

	public void setHealth(int health) {
		this.health = health;
	}

}
