package com.egs.survivalsim.game.entity.mob;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import com.egs.survivalsim.game.level.Level;
import com.egs.survivalsim.game.level.tiles.Tile;
import com.egs.survivalsim.util.InputHandler;

public class Player extends Mob{

	public Player(int id, int type, int x, int y, InputHandler input, Level level) {
		super(id, type, x, y, input, level);
	}

	public void render(Graphics g){
		g.setColor(Color.BLUE);
		g.setFont(new Font("Arial", Font.BOLD, 12));
		
//		if(getX() > (42 / 2) && getY() > (33 / 2) && getX() < level.getW() - (42 * 2) && getY() < level.getW()){
//			//System.out.println("rendeing pv1");
//			g.drawString("@", (42 / 2) * 12 + 2, (33 / 2) * 12 + 12);
//		}
//		else {
//			//System.out.println("Renderin pv2");
//			g.drawString("@", x * 12 + 2, y * 12 + 20);
//		}

		g.drawString("@", (42 / 2) * 12 + 2, (33 / 2) * 12 + 20);
		//g.drawString("@", x * 12 + 2, y * 12 + 20);

	}
	
	public void render(Graphics g, int xa, int ya){
		g.setColor(Color.BLUE);
		g.setFont(new Font("Arial", Font.BOLD, 12));
		
		g.drawString("@", xa * 12 + 2, ya * 12 + 20);
	}

	
	public void update(){
		
	}
	
	public void move(int xa, int ya){
		if(level.getTiles(getX(), ya) == Tile.grassTile || level.getTiles(getX(), ya) == Tile.sandTile || level.getTiles(getX(), ya) == Tile.snowTile) setY(ya);
		if(level.getTiles(xa, getY()) == Tile.grassTile || level.getTiles(xa, getY()) == Tile.sandTile || level.getTiles(xa, getY()) == Tile.snowTile) setX(xa);
		return;
	}
	
}
