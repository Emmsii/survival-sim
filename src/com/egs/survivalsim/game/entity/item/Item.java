package com.egs.survivalsim.game.entity.item;

import com.egs.survivalsim.game.entity.Entity;
import com.egs.survivalsim.game.level.Level;
import com.egs.survivalsim.util.InputHandler;

public class Item extends Entity{

	public Item(int id, int type, int x, int y, InputHandler input, Level level) {
		super(id, type, x, y, input, level);
	}

}
