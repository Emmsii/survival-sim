package com.egs.survivalsim.game.level;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import com.egs.survivalsim.MainComponent;
import com.egs.survivalsim.game.Time;
import com.egs.survivalsim.gui.GameInterface;
import com.egs.survivalsim.util.InputHandler;

public class LevelSimulation extends Level{
	
	public LevelSimulation(int w, int h, InputHandler input, GameInterface gameInterface, Time time, MainComponent main){
		super(w, h, input, gameInterface, time, main);
		worldGen = new WorldGen(this, gameInterface, w, h, random.nextInt(1000));
		worldGen.generateWorld();
	}
	
	public void render(Graphics g){	
		for(int y = 0; y < 33; y++){
			int yp = 0; 
			if((yp + yScroll) < h) yp = y + yScroll;
			else yp = h - yScroll;
			for(int x = 0; x < 42; x++){
				int xp = 0;
				if((xp + xScroll) < h) xp = x + xScroll;
				else xp = w - xScroll;
				
				int xPos = (x * 12) + 2;
				int yPos = (y * 12) + 24;
				getTiles(xp, yp).render(g, xPos, yPos);
			}
		}
		
		g.setColor(Color.WHITE);
		g.setFont(new Font("Arial", Font.PLAIN, 9));
		g.drawString("Simulation Mode", 437, 10);
		g.drawString("x: " + (xScroll + 42), 360, 10);
		g.drawString("y: " + (yScroll + 33), 398, 10);
		
		g.drawString("Average Tile Height: " + getAvHeight(), 510, 75);
		g.drawString("Water: < 80", 510, 86);
		g.drawString("Sand: >= 80 && < 83", 510, 97);
		g.drawString("Water Tiles: " + getWaterTiles(), 510, 130);
		g.drawString("Sand Tiles: " + getSandTiles(), 510, 141);
		g.drawString("Tree Tiles: " + getTreeTiles(), 510, 152);

	}
	
	public void update(){
		input();
		for(int y = 0; y < 33; y++){
			int yp = 0;
			if((yp + yScroll) < h) yp = y + yScroll;
			else yp = h - yScroll;
			for(int x = 0; x < 42; x++){
				int xp = 0;
				if((xp + xScroll) < w) xp = x + xScroll;
				else xp = x - xScroll;
				getTiles(xp, yp).update();
			}
		}
	}

}