package com.egs.survivalsim.game.level;

import java.awt.image.BufferedImage;
import java.nio.ByteBuffer;

import com.egs.survivalsim.util.FileHandler;

public class Noise {
	
	private FileHandler file;
	private int total;
	private int current;
	private int percent;	
	private int oldPercent;
	
	public Noise(){
		file = new FileHandler();
	}
	
	public double noise(int x, int y){
		x = x + y * 57;
		x = ((x << 13) ^ x);
		double t = (x * (x * x * 15731 + 789221)+ 1376312589) & 0x7fffffff;
		return 1 - t * 0.000000000931322574615478515625;
	}
	
	public double sNoise(int x, int y){
		double corners = (noise(x - 1, y - 1) + noise(x + 1, y - 1) + noise(x - 1, y + 1) + noise(x + 1, y + 1)) * 0.0625;
	    double sides = (noise(x - 1, y)  + noise(x + 1, y) + noise(x, y - 1) + noise(x, y + 1))  * 0.125;
	    double center = noise(x, y) * 0.25;
		return corners + sides + center;		
	}

	public double lInterpoleLin(double a, double b, double x){
		return a * (1 - x) + b * x;		
	}
	
	public double lInterpoleCos(double a, double b, double x){
		double ft = x * 3.1415927;
		double f = (1 - Math.cos(ft)) * 0.5;
		return a * f + b * (1 - f);
	}
	
	public double iNoise(double x, double y){
		int iX = (int) x;
		int iY = (int) y;
		double dX = x - iX;
		double dY = y - iY;
		double p1 = sNoise(iX, iY);
		double p2 = sNoise(iX + 1, iY);
		double p3 = sNoise(iX, iY + 1);
		double p4 = sNoise(iX + 1, iY + 1);
		double i1 = lInterpoleCos(p1 ,p2 ,dX);
		double i2 = lInterpoleCos(p3, p4, dX);
		return lInterpoleCos(i1, i2, dY);	
	} 	
	
	public double pNoise(double x, double y, double persistence, int octave, double amplitude){
		double result;
		int frequence = 1;
		result = 0;
		for(int n = 0; n < octave; n++){
			frequence <<= 1;
			amplitude *= persistence;
			result += iNoise(x * frequence, y * frequence) * amplitude;
		}
		return result * (persistence - 1) / (Math.pow(persistence, octave + 1) - 1);	
	}

	public void startNoise(int w, int h, int seed, double size, double percistence, int octave, double amplitude, String name, int world){		
		System.out.println("Generating noise map");
		total = w * h;
		file.newChunkFolder(world);
		file.newHeightFolder(world);
		
		double max = 0D;
		int width = w + seed;
		int height = h + seed;
		
		
		
		double[][] chunk = new double[16][16];
		int posChunk = w / 16;
		int itteration = 1;
		
		System.out.println("SIZE: " + w + "x" + h + ", POSSIBLE CHUNKS: " + posChunk);
		
		int cx = -1;
		int cy = -1;
		
		for(int y = 0; y < (16 * itteration); y++){
			if(cy >= 15) cy = 0;
			else cy++;
			for(int x = 0; x < (16 * itteration); x++){	
				System.out.println("Itteration: " + itteration);
				if(cx >= 15) cx = 0;
				else cx++;
				System.out.println("X: " + x + ", Y: " + y + ", CX: " + cx + ", CY: " + cy);;
				if(itteration >= posChunk) break;
				
				if(cx >= 15 && cy >= 15){
					System.out.println("THEY EQUAL 15");
					System.out.println("Itteration: " + itteration);
					System.out.println("15 IF: CX: " + cx + ", CY: " + cy);
					
					itteration++;
				}
				
				//NOISE GO HERE

			}
			
			
		} 
		
	}
	
	private void makeChunk(double[][] data){
		System.out.println("IN MAKE CHUNK METHOD");
		byte[] result = new byte[8];
		for(int y = 0; y < data.length; y++){
			for(int x = 0; x < data.length; x++){
				result = toByteArray(data[x][y]);
				file.saveChunk(result, file.getWorlds());
				result = new byte[8];
			}
		}
		
	}
	
	public static byte[] toByteArray(double value){
		byte[] bytes = new byte[8];
		ByteBuffer.wrap(bytes).putDouble(value);
		System.out.println("BYTE: " + bytes);
		return bytes;
	}
	
	
	private double normalize(double a, double max){
		return Math.abs(a / max);
	}
	
	private void calcPercent(){
		percent = (current * 100) / total;
	}
	
	private void printPercent(){
		if(oldPercent != percent){
			System.out.println(percent + "%");
			oldPercent = percent;
		}
		
	}
	
	
}
