package com.egs.survivalsim.game.level;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import com.egs.survivalsim.MainComponent;
import com.egs.survivalsim.game.Time;
import com.egs.survivalsim.game.entity.mob.Player;
import com.egs.survivalsim.game.level.tiles.Tile;
import com.egs.survivalsim.gui.GameInterface;
import com.egs.survivalsim.util.FileHandler;
import com.egs.survivalsim.util.InputHandler;

public class LevelAdventure extends Level{

	private Player player;
	private FileHandler file;

	private int spawnX = 40;
	private int spawnY = 40;
	
	private boolean debug = false;
	
	public LevelAdventure(int w, int h, InputHandler input, GameInterface gameInterface, Time time, MainComponent main) {
		super(w, h, input, gameInterface, time, main);

		
		worldGen = new WorldGen(this, gameInterface, w, h, random.nextInt(1000));
		worldGen.generateWorld();
		

		//setSpawn();
		player = new Player(0, 0, spawnX, spawnY, input, this);
	}
	
	public void render(Graphics g){
		for(int y = 0; y < 33; y++){
			int yp = 0; 
			if((yp + yScroll) < h) yp = y + yScroll;
			else yp = h - yScroll;
			for(int x = 0; x < 42; x++){
				int xp = 0;
				if((xp + xScroll) < h) xp = x + xScroll;
				else xp = w - xScroll;
				
				int xPos = (x * 12) + 2;
				int yPos = (y * 12) + 24;
				
				if(!debug) getTiles(xp, yp).render(g, xPos, yPos);
				else if(debug) getTiles(xp, yp).renderDebug(g, xPos, yPos);
			}
		}

		
		player.render(g);
		
		g.setColor(Color.WHITE);
		g.setFont(new Font("Arial", Font.PLAIN, 9));
		g.drawString("Adventure Mode", 437, 10);
		g.drawString("xp: " + player.getX(), 360, 10);
		g.drawString("yp: " + player.getY(), 398, 10);
		g.drawString("x: " + (xScroll), 360, 20);
		g.drawString("y: " + (yScroll), 398, 20);
		
		
		
		for(int y = 0; y < 128; y++){
			for(int x = 0; x < 128; x++){
				int xPos = x + 586;
				int yPos = y + 12 ;
			
				if(getTiles(x, y) == Tile.grassTile){
					g.setColor(new Color(75, 94, 45));
					g.drawRect(xPos, yPos, 1, 1);
				}
				if(getTiles(x, y) == Tile.waterShallowTile){
					g.setColor(new Color(48, 93, 156));
					g.drawRect(xPos, yPos, 1, 1);
				}
				if(getTiles(x, y) == Tile.waterDeepTile){
					g.setColor(new Color(33, 62, 102));
					g.drawRect(xPos, yPos, 1, 1);
				}
				if(getTiles(x, y) == Tile.sandTile){
					g.setColor(new Color(189, 179, 142));
					g.drawRect(xPos, yPos, 1, 1);
				}
				if(getTiles(x, y) == Tile.treeTile){
					g.setColor(new Color(56, 33, 27));
					g.drawRect(xPos, yPos, 1, 1);
				}
				if(getTiles(x, y) == Tile.rockTile){
					g.setColor(new Color(150, 150, 150));
					g.drawRect(xPos, yPos, 1, 1);
				}
				if(getTiles(x, y) == Tile.snowTile){
					g.setColor(new Color(206, 226, 235));
					g.drawRect(xPos, yPos, 1, 1);
				}
				if(getTiles(x, y) == Tile.iceTile){
					g.setColor(new Color(159, 180, 196));
					g.drawRect(xPos, yPos, 1, 1);
				}
				
				
			}
		}
		g.setColor(Color.WHITE);
		g.drawRect(player.getX() + 586, player.getY() + 12, 1, 1);
		
		g.setColor(Color.WHITE);
		g.drawRect(586, 12, 128, 128);
	}
	

	public void update(){
		xScroll = player.getX() - (42 / 2);
		yScroll = player.getY() - (33 / 2);
				
		if(input.up.isPressed()) player.move(player.getX(), player.getY() - 1);
		if(input.down.isPressed()) player.move(player.getX(), player.getY() + 1);
		if(input.left.isPressed()) player.move(player.getX() - 1, player.getY());
		if(input.right.isPressed()) player.move(player.getX() + 1, player.getY());
		
		if(input.debug.isPressed()){
			System.out.println("DEBUG!");
			if(!debug) debug = true;
			else if(debug) debug = false;
		}
	}
	
	public void input(){
		if(input.up.isPressed()){
			if(yScroll <= 0) yScroll = 0;
			else yScroll--;
		}
		if(input.down.isPressed()){
			if(yScroll >= (h - 34)) yScroll = (h - 34);
			else yScroll++;
		}
		if(input.left.isPressed()){
			if(xScroll <= 0) xScroll = 0;
			else xScroll--;
		}
		if(input.right.isPressed()){
			if(xScroll >= (w - 43)) xScroll = (w - 43);
			else xScroll++;
		}
		
		
 	}
		
	public void setSpawn(){
		System.out.println("Setting spawn");
		boolean accepted = false;
		while(!accepted){
			int guessX = random.nextInt(w);
			int guessY = random.nextInt(h);
			if(guessX > (42 / 2) && guessY > (33 / 2) && guessX < (w - (42 / 1)) && guessY < (h - (33 /2))){
				if(getTiles(guessX, guessY) == Tile.grassTile || getTiles(guessX, guessY) == Tile.sandTile){
					System.out.println(getTiles(guessX, guessY));
					accepted = true;
					spawnX = guessX;
					spawnY = guessY;
				}else{
					accepted = false;
					System.out.println("Spawn Rejected: Tile " + getTiles(guessX, guessY) + " was not sand or grass");
				}
			}else{
				accepted = false;
				System.out.println("Spawn Rejected: Out of boundsd");
			}
		}
	}
}
