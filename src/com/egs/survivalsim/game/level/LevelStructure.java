package com.egs.survivalsim.game.level;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import com.egs.survivalsim.MainComponent;
import com.egs.survivalsim.game.Time;
import com.egs.survivalsim.gui.GameInterface;
import com.egs.survivalsim.util.InputHandler;

public class LevelStructure extends Level{
	
	private int size;
	private String name;
	
	public LevelStructure(int w, int h, int size, String structName, InputHandler input, GameInterface gameInterface, Time time, MainComponent main) {
		super(w, h, input, gameInterface, time, main);
		this.size = size;
		this.name = structName;
		
		generateTiles();
	}
	
	public void render(Graphics g){
		g.setColor(Color.WHITE);
		g.setFont(new Font("Arial", Font.PLAIN, 9));
		g.drawString("Structure Editor", 437, 10);
		
		drawText(name, 510, 30, 12, true, g);
		if(size == 0) drawText("8x8", 510, 41, 12, true, g);
		if(size == 1) drawText("16x16", 510, 41, 12, true, g);
		if(size == 2) drawText("32x32", 510, 41, 12, true, g);
		if(size == 3) drawText("32x32", 510, 41, 12, true, g);
		
		for(int y = 0; y < 33; y++){
			for(int x = 0; x < 42; x++){
				getTiles(x, y).render(g, x * 12 + 2, y * 12 + 24);
			}
		}

	}
	
	public void update(){
		input();
	}
	
	public void input(){
		
	}
	
	public void drawText(String msg, int x, int y, int size, boolean bold, Graphics g){
		if(!bold) g.setFont(new Font("Arial", Font.PLAIN, size));
		if(bold) g.setFont(new Font("Arial", Font.BOLD, size));
		
		g.setColor(Color.BLACK);
		g.drawString(msg, x + 1, y + 1);
		g.setColor(Color.WHITE);
		g.drawString(msg, x, y);
	}
	
	public void generateTiles(){
		int nw = 0;
		int nh = 0;
		if(size == 0){
			nw = 8; 
			nh = 8;
		}
		if(size == 1){
			nw = 16; 
			nh = 16;
		}
		if(size == 2){ 
			nw = 32; 
			nh = 32;
		}
		if(size == 3){
			nw = 64;
			nh = 64;
		}
		
		for(int y = 0; y < nh; y++){
			for(int x = 0; x < nw; x++){
				setTiles(x, y, -1);
			}
		}
	}
}
