package com.egs.survivalsim.game.level;

import java.awt.Graphics;
import java.util.Random;

import com.egs.survivalsim.MainComponent;
import com.egs.survivalsim.game.Time;
import com.egs.survivalsim.game.level.tiles.Tile;
import com.egs.survivalsim.gui.GameInterface;
import com.egs.survivalsim.util.InputHandler;

public class Level {

	protected Random random = new Random();
	protected InputHandler input;
	protected GameInterface gameInterface;
	protected WorldGen worldGen;
	protected Time time;
	protected MainComponent main;
	
	protected int w;
	protected int h;
	protected int xScroll = 0;
	protected int yScroll = 0;
	
	//TODO TEMP VALUE
	protected double avHeight = 0;
	protected int treeTiles = 0;
	protected int waterTiles = 0;
	protected int sandTiles = 0;
	
	protected int[] tiles;
	
 	public Level(int w, int h, InputHandler input, GameInterface gameInterface, Time time, MainComponent main){
		this.w = w;
		this.h = h;
		this.input = input;
		this.gameInterface = gameInterface;
		this.time = time;
		this.main = main;
		tiles = new int[w * h];
		
	}
	
	public void render(Graphics g){
		
	}
	
	public void update(){
	}
	
	public void input(){
		if(input.up.isPressed()){
			if(yScroll <= 0) yScroll = 0;
			else yScroll--;
		}
		if(input.down.isPressed()){
			if(yScroll >= (h - 34)) yScroll = (h - 34);
			else yScroll++;
		}
		if(input.left.isPressed()){
			if(xScroll <= 0) xScroll = 0;
			else xScroll--;
		}
		if(input.right.isPressed()){
			if(xScroll >= (w - 43)) xScroll = (w - 43);
			else xScroll++;
		}
	}
	
	public void reset(){
		System.out.println("Resetting");
		w = 0;
		h = 0;
		xScroll = 0;
		yScroll = 0;
		gameInterface = null;
		input = null;
		time = null;
		main = null;
		System.out.println("Resetting complete");
	}
	
	public Tile getTiles(int x, int y){
		if(x < 0 || y < 0 || x >= w || y >= h) return Tile.voidTile;
		if(tiles[x + y * w] == -1) return Tile.emptyTile;
		if(tiles[x + y * w] == 0) return Tile.grassTile;
		if(tiles[x + y * w] == 1) return Tile.treeTile;
		if(tiles[x + y * w] == 2) return Tile.waterShallowTile;
		if(tiles[x + y * w] == 3) return Tile.waterDeepTile;
		if(tiles[x + y * w] == 4) return Tile.sandTile;
		if(tiles[x + y * w] == 5) return Tile.rockTile;
		if(tiles[x + y * w] == 6) return Tile.snowTile;
		if(tiles[x + y * w] == 7) return Tile.iceTile;
		if(tiles[x + y * w] == 8) return Tile.riverTile;
		if(tiles[x + y * w] == 9) return Tile.pineTile;
		if(tiles[x + y * w] == 10) return Tile.jungleTile;
		return Tile.voidTile;
	}
	
	public int getW() {
		return w;
	}

	public void setW(int w) {
		this.w = w;
	}

	public int getH() {
		return h;
	}

	public void setH(int h) {
		this.h = h;
	}

	public void setTiles(int x, int y, int tile){
		tiles[x + y * w] = tile;
	}

	public int getxScroll() {
		return xScroll;
	}

	public void setxScroll(int xScroll) {
		this.xScroll = xScroll;
	}

	public int getyScroll() {
		return yScroll;
	}

	public void setyScroll(int yScroll) {
		this.yScroll = yScroll;
	}

	public double getAvHeight() {
		return avHeight;
	}

	public void setAvHeight(double avHeight) {
		this.avHeight = avHeight;
	}

	public WorldGen getWorldGen() {
		return worldGen;
	}

	public void setWorldGen(WorldGen worldGen) {
		this.worldGen = worldGen;
	}
	
	public int getTreeTiles() {
		return treeTiles;
	}

	public void setTreeTiles(int treeTiles) {
		this.treeTiles = treeTiles;
	}

	public int getSandTiles() {
		return sandTiles;
	}

	public void setSandTiles(int sandTiles) {
		this.sandTiles = sandTiles;
	}

	public int getWaterTiles() {
		return waterTiles;
	}

	public void setWaterTiles(int waterTiles) {
		this.waterTiles = waterTiles;
	}

}
