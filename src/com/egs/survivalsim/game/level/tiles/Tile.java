package com.egs.survivalsim.game.level.tiles;

import java.awt.Graphics;

import com.egs.survivalsim.game.level.tiles.trees.JungleTreeTile;
import com.egs.survivalsim.game.level.tiles.trees.PineTile;
import com.egs.survivalsim.game.level.tiles.trees.TreeTile;

public class Tile {

	protected int x, y;
	protected double height;
	protected double temp;
	protected int equDist;
	protected int biome;
	
	public static Tile emptyTile = new EmptyTile();
	public static Tile voidTile = new VoidTile();
	public static Tile grassTile = new GrassTile();
	public static Tile treeTile = new TreeTile();
	public static Tile waterShallowTile = new WaterShallowTile();
	public static Tile waterDeepTile = new WaterDeepTile();
	public static Tile sandTile = new SandTile();
	public static Tile rockTile = new RockTile();
	public static Tile snowTile = new SnowTile();
	public static Tile iceTile = new IceTile();
	public static Tile riverTile = new RiverTile();
	public static Tile jungleTile = new JungleTreeTile();
	public static Tile pineTile = new PineTile();
	
	public Tile(){

	}
	
	public void render(Graphics g, int x, int y){
		
	}
	
	public void renderDebug(Graphics g, int x, int y){
		
	}
	
	public void update(){
		
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public int getEquDist() {
		return equDist;
	}

	public void setEquDist(int equDist) {
		this.equDist = equDist;
	}

	public double getTemp() {
		return temp;
	}

	public void setTemp(double temp) {
		this.temp = temp;
	}

	public int getBiome() {
		return biome;
	}

	public void setBiome(int biome) {
		this.biome = biome;
	}
}
