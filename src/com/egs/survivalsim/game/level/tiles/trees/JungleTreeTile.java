package com.egs.survivalsim.game.level.tiles.trees;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import com.egs.survivalsim.game.level.tiles.Tile;

public class JungleTreeTile extends Tile{

	public JungleTreeTile(){
		
	}
	
	public void render(Graphics g, int x, int y){
		g.setColor(new Color(55, 94, 25)); 
		g.fillRect(x, y - 12, 12, 12);
		
		g.setFont(new Font("Arial", Font.BOLD, 13));
		g.setColor(new Color(23, 43, 8)); 
		g.drawString("T", x + 2, y - 1);
		
		g.setFont(new Font("Arial", Font.BOLD, 9));
		g.drawString("|", x + 2, y - 3);
		g.setFont(new Font("Arial", Font.BOLD, 9));
		g.drawString("|", x + 7, y - 3);
		
		
		
		g.setFont(new Font("Arial", Font.BOLD, 13));
		g.setColor(new Color(66, 43, 27)); 
		g.drawString("T", x + 2, y);
		g.setFont(new Font("Arial", Font.PLAIN, 12));
	}
}
