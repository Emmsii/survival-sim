package com.egs.survivalsim.game.level.tiles;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

public class VoidTile extends Tile{

	public VoidTile(){
		
	}
	
	public void render(Graphics g, int x, int y){
		g.setColor(Color.RED);
		g.setFont(new Font("Arial", Font.PLAIN, 11));
		
		g.drawString("X", x + 2, y - 1);
	}
}
