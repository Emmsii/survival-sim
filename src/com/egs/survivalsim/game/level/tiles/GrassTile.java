package com.egs.survivalsim.game.level.tiles;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

public class GrassTile extends Tile{

	public GrassTile(){

	}
	
	public void render(Graphics g, int x, int y){
		g.setColor(new Color(55, 94, 25)); 
		g.fillRect(x, y - 12, 12, 12);
			
		g.setColor(new Color(19, 33, 9));
		g.setFont(new Font("Arial", Font.BOLD, 12));
		g.drawString("' '", x + 2, y + 5);
	}
	
	public void renderDebug(Graphics g, int x, int y){
		g.setColor(Color.WHITE);
		g.setFont(new Font("Arial", Font.PLAIN, 9));
		
		g.drawString(Integer.toString(getBiome()), x, y);
	}
	
	public void update(){
		
	}
}
