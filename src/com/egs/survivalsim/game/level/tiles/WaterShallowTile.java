package com.egs.survivalsim.game.level.tiles;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

public class WaterShallowTile extends Tile{

	public WaterShallowTile(){
		
	}
	
	public void render(Graphics g, int x, int y){
		g.setColor(new Color(48, 93, 156)); 
		g.fillRect(x, y - 12, 12, 12);
		
		g.setFont(new Font("Arial", Font.BOLD, 11));
		g.setColor(new Color(67, 133, 186)); 
		g.drawString("W", x + 1, y - 2);
	}
	
	public void renderDebug(Graphics g, int x, int y){
		g.setColor(Color.WHITE);
		g.setFont(new Font("Arial", Font.PLAIN, 9));
		
		g.drawString(Double.toString(getHeight()).substring(0, 3), x, y);
	}
}
