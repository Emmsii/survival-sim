package com.egs.survivalsim.game.level.tiles;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

public class RockTile extends Tile{

	public RockTile(){
		
	}
	
	public void render(Graphics g, int x, int y){
		g.setColor(new Color(55, 94, 25)); 
		g.fillRect(x, y - 12, 12, 12);
		
		g.setFont(new Font("Arial", Font.BOLD, 13));
		g.setColor(new Color(150, 150, 150)); 
		g.drawString("R", x, y);
		
	}
}
