package com.egs.survivalsim.game.level.tiles;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

public class SandTile extends Tile{

	public SandTile(){
		
	}
	
	public void render(Graphics g, int x, int y){
		g.setColor(new Color(189, 179, 142)); 
		g.fillRect(x, y - 12, 12, 12);
		
		g.setFont(new Font("Arial", Font.BOLD, 13));
		g.setColor(new Color(234, 220, 164)); 
		g.drawString("~", x + 2, y - 5);
		g.drawString("~", x + 2, y + 1);
		g.setFont(new Font("Arial", Font.PLAIN, 12));
	}
	
	public void renderDebug(Graphics g, int x, int y){
		g.setColor(Color.WHITE);
		g.setFont(new Font("Arial", Font.PLAIN, 9));
		
		g.drawString(Double.toString(getHeight()).substring(0, 3), x, y);
	}
}
