package com.egs.survivalsim.game.level.tiles;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

public class RiverTile extends Tile{

	public RiverTile(){
		
	}
	
	public void render(Graphics g, int x, int y){
		g.setColor(new Color(48, 93, 156)); 
		g.fillRect(x, y - 12, 12, 12);
		
		g.setFont(new Font("Arial", Font.BOLD, 11));
		g.setColor(new Color(67, 133, 186)); 
		g.drawString("W", x + 1, y - 2);
	}
}
