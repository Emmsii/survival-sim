package com.egs.survivalsim.game.level.tiles;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

public class EmptyTile extends Tile{

	public EmptyTile(){
		
	}
	
	public void render(Graphics g, int x, int y){
		g.setColor(new Color(100, 100, 100));
		g.setFont(new Font("Arial", Font.PLAIN, 12));
//		g.drawString("|", x - 1, y - 1);
//		g.drawString("|", x + 11, y - 1);
		g.drawString("[  ]", x + 1, y - 1);
	}
}
