package com.egs.survivalsim.game.level;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Random;

import javax.imageio.ImageIO;

import com.egs.survivalsim.game.level.tiles.Tile;
import com.egs.survivalsim.gui.GameInterface;
import com.egs.survivalsim.util.FileHandler;

public class WorldGen {

	private Random random = new Random();
	private Noise noise;
	private Level level;
	private FileHandler file;
	private GameInterface gameInterface;
	
	private int w, h;
	private int seed;
	private int rivers = 0;

	private double[][] heightMap;
	private double[][] tempMap;
	private int[][] biomeMap;
	private int[][] elevationMap;
	
	private double maxHeight;
	
	private boolean rejected = true;
	
	public WorldGen(Level level, GameInterface gameInterface, int w, int h, int seed){
		this.level = level;
		this.gameInterface = gameInterface;
		this.w = w;
		this.h = h;
		this.seed = seed;
		file = new FileHandler();
	}
	
	public void generateWorld(){
		System.out.println("NEW WORLD");
		while(rejected){
			double start = System.nanoTime();
			
			if(!file.checkWorlds()) return;
			file.makeWorldFolder();
			
			System.out.println("Starting noise");
			noise = new Noise();
			noise.startNoise(w, h, seed, 0.004, 0.4, 8, 16, "height_map", file.getWorlds());
			
			heightMap = new double[w][h];
			tempMap = new double[w][h];
			biomeMap = new int[w][h];
			elevationMap = new int[w][h];
			//heightMap = noise.startNoise(w, h, seed, 0.004, 0.4, 8, 16, "height_map", file.getWorlds());
			
			generateLand();
			generateTemp();
			generateElevation();
			generateBiomes();
			populateBiomes();
			
			saveWorldImage(file.getWorlds()); //TODO REMOVE WHEN EXPORTING
			saveHeightImage(file.getWorlds());
			saveBiomeImage(file.getWorlds());
			saveTempImage(file.getWorlds());
			
			rejected = false;
			double timeTaken = (System.nanoTime() - start) / 1000000;
			gameInterface.addConsole("World Size: " + w + "x" + h);
			gameInterface.addConsole("Generated in: " + (int) timeTaken + "ms");
			gameInterface.addConsole("Rivers: " + rivers);
		}
	}
	
	public void generateLand(){
		for(int y = 0; y < h; y++){
			for(int x = 0; x < w; x++){
				level.setTiles(x, y, 0);
				
				if(heightMap[x][y] >= 0.2D && heightMap[x][y] < 0.22){
					level.setTiles(x, y, 4);
				}
				
				if(heightMap[x][y] < 0.2D){
					level.setTiles(x, y, 2);
				}
				
				if(heightMap[x][y] < 0.14D){
					level.setTiles(x, y, 3);
				}
				
				if(heightMap[x][y] > maxHeight) maxHeight = heightMap[x][y];
			}
		}
	}
	
	public void generateTemp(){
		System.out.println("Setting temperature");
		int equator = h / 2;
		int distToEqu = 0;
		
		for(int y = 0; y < h; y++){
			for(int x = 0; x < w; x++){
				distToEqu = (equator - y);
				if(distToEqu < 0) distToEqu = distToEqu * -1;
				double result = 0;
				
				if(w == 128) result = normalize(distToEqu + (heightMap[x][y] * 10), equator + maxHeight);
				if(w == 256) result = normalize(distToEqu + (heightMap[x][y] * 50), equator + maxHeight);
				if(w == 512) result = normalize(distToEqu + (heightMap[x][y] * 75), equator + maxHeight);
				if(w == 1024) result = normalize(distToEqu + (heightMap[x][y] * 200), equator + maxHeight);
				if(w == 2048) result = normalize(distToEqu + (heightMap[x][y] * 450), equator + maxHeight);
				if(w == 4096) result = normalize(distToEqu + (heightMap[x][y] * 500), equator + maxHeight);
				if(w == 8192) result = normalize(distToEqu + (heightMap[x][y] * 550), equator + maxHeight);
				tempMap[x][y] = result;
				//System.out.println("TEMP: " + result);
				level.getTiles(x, y).setTemp(result);
			}
		}
	}
	
	public void generateElevation(){
		System.out.println("Generating Elevation Map");
		
		for(int y = 0; y < h; y++){
			for(int x = 0; x < w; x++){				
				if(heightMap[x][y] >= 0.85) elevationMap[x][y] = 4;
				if(heightMap[x][y] >= 0.5 && heightMap[x][y] < 0.85) elevationMap[x][y] = 3;
				if(heightMap[x][y] >= 0.35 && heightMap[x][y] < 0.5) elevationMap[x][y] = 2;
				if(heightMap[x][y] >= 0.2 && heightMap[x][y] < 0.35) elevationMap[x][y] = 1;	
				if(heightMap[x][y] < 0.2) elevationMap[x][y] = 5;
			}
		}
	}

	public void generateBiomes(){
		System.out.println("Generating biomes");
		for(int y = 0; y < h; y++){
			for(int x = 0; x < w; x++){
				float rand = random.nextFloat() / 32;
				
				if(elevationMap[x][y] == 4){
					if(tempMap[x][y] >= 0.85 + rand) biomeMap[x][y] = 1;
					else if(tempMap[x][y] < 0.85 + rand && tempMap[x][y] >= 0.685 + rand) biomeMap[x][y] = 2;
					else if(tempMap[x][y] < 0.685 + rand && tempMap[x][y] >= 0.42 + rand) biomeMap[x][y] = 3;
					else if(tempMap[x][y] < 0.42 + rand && tempMap[x][y] >= 0.3 + rand) biomeMap[x][y] = 4;
					else if(tempMap[x][y] < 0.3 + rand) biomeMap[x][y] = 5;
				}else if(elevationMap[x][y] == 3){
					if(tempMap[x][y] >= 0.85 + rand) biomeMap[x][y] = 1;
					else if(tempMap[x][y] < 0.85 + rand && tempMap[x][y] >= 0.685 + rand) biomeMap[x][y] = 2;
					else if(tempMap[x][y] < 0.685 + rand && tempMap[x][y] >= 0.42 + rand) biomeMap[x][y] = 3;
					else if(tempMap[x][y] < 0.42 + rand && tempMap[x][y] >= 0.23 + rand) biomeMap[x][y] = 4;
					else if(tempMap[x][y] < 0.3 + rand) biomeMap[x][y] = 5;
				}else if(elevationMap[x][y] == 2){
					if(tempMap[x][y] >= 0.85 + rand) biomeMap[x][y] = 1;
					else if(tempMap[x][y] < 0.85 + rand && tempMap[x][y] >= 0.685 + rand) biomeMap[x][y] = 2;
					else if(tempMap[x][y] < 0.685 + rand && tempMap[x][y] >= 0.42 + rand) biomeMap[x][y] = 3;
					else if(tempMap[x][y] < 0.42 + rand && tempMap[x][y] >= 0.3 + rand) biomeMap[x][y] = 4;
					else if(tempMap[x][y] < 0.3 + rand) biomeMap[x][y] = 5;
				}else if(elevationMap[x][y] == 1){
					if(tempMap[x][y] >= 0.85 + rand) biomeMap[x][y] = 1;
					else if(tempMap[x][y] < 0.85 + rand && tempMap[x][y] >= 0.685 + rand) biomeMap[x][y] = 2;
					else if(tempMap[x][y] < 0.685 + rand && tempMap[x][y] >= 0.42) biomeMap[x][y] = 3;
					else if(tempMap[x][y] < 0.42 + rand && tempMap[x][y] >= 0.3 + rand) biomeMap[x][y] = 4;
					else if(tempMap[x][y] < 0.3 + rand) biomeMap[x][y] = 5;
				}
			}
		}
	}
	
	public void populateBiomes(){
		System.out.println("Populating biomes");
		for(int y = 0; y < h; y++){
			for(int x = 0; x < w; x++){
				if(biomeMap[x][y] == 1){
					level.setTiles(x, y, 6);
					level.getTiles(x, y).setBiome(1);
					
				}else if(biomeMap[x][y] == 2){
					if(random.nextInt(10) == 0){
						if(level.getTiles(x, y) == Tile.grassTile) level.setTiles(x, y, 9);
					}
					level.getTiles(x, y).setBiome(2);
					
				}else if(biomeMap[x][y] == 3){					
					if(random.nextInt(15) == 0){
						if(level.getTiles(x, y) == Tile.grassTile) level.setTiles(x, y, 1);
					}
					level.getTiles(x, y).setBiome(3);
					
				}else if(biomeMap[x][y] == 4){
					if(random.nextInt(6) == 0){
						if(level.getTiles(x, y) == Tile.grassTile) level.setTiles(x, y, 10);
					}
					level.getTiles(x, y).setBiome(4);
				}else if(biomeMap[x][y] == 5){					
					level.setTiles(x, y, 4);
					level.getTiles(x, y).setBiome(5);
				}
			}
		}
	}
	
	public boolean checkBounds(int x, int y){
		if(x <= 0 || y <= 0 || y >= h || x >= w){
			System.out.println("Out of bounds");
			return true;
		}
		//System.out.println("X: " + x + ", Y: " + y + " not out of bounds");
		return false;
	}	

	public double normalize(double a, double max){
		return Math.abs(a / max);
	}
	
	public void saveWorldImage(int world){
		System.out.println("Saving world image");
		
		BufferedImage image = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
		for(int i = 0; i < w; i++){
			for(int j = 0; j < h; j++){
				if(level.getTiles(i, j) == Tile.grassTile){
					int c= new Color(110, 182, 62).getRGB();	
					image.setRGB(i, j, c);
				}else if(level.getTiles(i, j) == Tile.treeTile){
					int c = new Color(50, 73, 36).getRGB();
					image.setRGB(i, j, c);
				}else if(level.getTiles(i, j) == Tile.rockTile){
					int c = new Color(92, 92, 92).getRGB();
					image.setRGB(i, j, c);
				}else if(level.getTiles(i, j) == Tile.waterShallowTile){
					int c = new Color(61, 140, 196).getRGB();
					image.setRGB(i, j, c);
				}else if(level.getTiles(i, j) == Tile.waterDeepTile){
					int c = new Color(41, 120, 156).getRGB();
					image.setRGB(i, j, c);
				}else if(level.getTiles(i, j) == Tile.sandTile){
					int c = new Color(189, 179, 142).getRGB();
					image.setRGB(i, j, c);
				}else if(level.getTiles(i, j) == Tile.snowTile){
					int c = new Color(230, 230, 230).getRGB();
					image.setRGB(i, j, c);
				}else if(level.getTiles(i, j) == Tile.iceTile){
					int c = new Color(198, 224, 245).getRGB();
					image.setRGB(i, j, c);
				}else if(level.getTiles(i, j) == Tile.riverTile){
					int c = new Color(81, 145, 206).getRGB();
					image.setRGB(i, j, c);
				}else if(level.getTiles(i, j) == Tile.jungleTile){
					int c = new Color(79, 140, 0).getRGB();
					image.setRGB(i, j, c);
				}else if(level.getTiles(i, j) == Tile.pineTile){
					int c = new Color(98, 156, 115).getRGB();
					image.setRGB(i, j, c);
				}
			}
		}
		String location = System.getenv("APPDATA");
		File fileImage = new File(location + "/.survivalsim/world_" + world + "/world.png");
		try {
			ImageIO.write(image, "png", fileImage);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void saveHeightImage(int world){
		System.out.println("Saving height image");
		
		BufferedImage image = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
		for(int i = 0; i < w; i++){
			for(int j = 0; j < h; j++){
				if(elevationMap[i][j] == 1){
					int c = new Color(189, 189, 189).getRGB();
					image.setRGB(i, j, c);
				}else if(elevationMap[i][j] == 2){
					int c = new Color(125, 125, 125).getRGB();
					image.setRGB(i, j, c);
				}else if(elevationMap[i][j] == 3){
					int c = new Color(54, 54, 54).getRGB();
					image.setRGB(i, j, c);
				}else if(elevationMap[i][j] == 4){
					int c = new Color(0, 0, 0).getRGB();
					image.setRGB(i, j, c);
				}
				else if(elevationMap[i][j] == 5){
					int c = new Color(255, 0, 0).getRGB();
					image.setRGB(i, j, c);
				}
			}
		}
		
		String location = System.getenv("APPDATA");
		File fileImage = new File(location + "/.survivalsim/world_" + world + "/world_height.png");
		try {
			ImageIO.write(image, "png", fileImage);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void saveTempImage(int world){
		System.out.println("Saving temperature image");
		
		BufferedImage image = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
		for(int i = 0; i < w; i++){
			for(int j = 0; j < h; j++){
				if(tempMap[i][j] >= 1.0){
					int c = new Color(119, 226, 237).getRGB();
					image.setRGB(i, j, c);
				}else if(tempMap[i][j] < 1.0 && tempMap[i][j] >= 0.685){
					int c = new Color(76, 212, 133).getRGB();
					image.setRGB(i, j, c);
				}else if(tempMap[i][j] < 0.685 && tempMap[i][j] >= 0.42){
					int c = new Color(164, 204, 78).getRGB();
					image.setRGB(i, j, c);
				}else if(tempMap[i][j] < 0.42 && tempMap[i][j] >= 0.23){
					int c = new Color(84, 143, 24).getRGB();
					image.setRGB(i, j, c);
				}else if(tempMap[i][j] < 0.23){
					int c = new Color(222, 120, 69).getRGB();
					image.setRGB(i, j, c);
				}
			}
		}
		
		String location = System.getenv("APPDATA");
		File fileImage = new File(location + "/.survivalsim/world_" + world + "/world_temp.png");
		try {
			ImageIO.write(image, "png", fileImage);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void saveBiomeImage(int world){
		System.out.println("Saving biome image");
		
		BufferedImage image = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
		for(int i = 0; i < w; i++){
			for(int j = 0; j < h; j++){
				if(biomeMap[i][j] == 1){
					int c = new Color(126, 183, 214).getRGB();
					image.setRGB(i, j, c);
				}else if(biomeMap[i][j] == 2){
					int c = new Color(101, 143, 90).getRGB();
					image.setRGB(i, j, c);
				}else if(biomeMap[i][j] == 3){
					int c = new Color(142, 194, 112).getRGB();
					image.setRGB(i, j, c);
				}else if(biomeMap[i][j] == 4){
					int c = new Color(67, 130, 8).getRGB();
					image.setRGB(i, j, c);
				}else if(biomeMap[i][j] == 5){
					int c = new Color(212, 184, 123).getRGB();
					image.setRGB(i, j, c);
				}
				
			}
		}
		String location = System.getenv("APPDATA");
		File fileImage = new File(location + "/.survivalsim/world_" + world + "/world_biomes.png");
		try {
			ImageIO.write(image, "png", fileImage);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
