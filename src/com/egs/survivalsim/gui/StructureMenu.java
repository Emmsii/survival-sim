package com.egs.survivalsim.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import javax.swing.JOptionPane;

import com.egs.survivalsim.MainComponent;
import com.egs.survivalsim.util.InputHandler;

public class StructureMenu {

	private InputHandler input;
	private MainComponent main;
	private Menu menu;

	private String[] sizes = {"Tiny", "Small", "Medium", "Large"};
	private String name = null;
	
	private int selection = 0;
	private int sizesSelection = 0;
	
	public StructureMenu(InputHandler input, Menu menu, MainComponent main){
		this.input = input;
		this.menu = menu;
		this.main = main;
	

	}
	
	public void render(Graphics g){
		drawText("Structure Menu", 30, 40, 20, true, g);
		drawText("Create a structure that will be randomly generated in simulation & adventure modes.", 32, 52, 11, true, g);
		
		drawText("Structure Size", 50, 75, 12, true, g);
		
		if(selection == 0) drawText(">", 40, 75, 12, true, g);
		
		if(sizesSelection == 0) drawText("(8x8)", 135, 75, 10, true, g);
		if(sizesSelection == 1) drawText("(16x16)", 135, 75, 10, true, g);
		if(sizesSelection == 2) drawText("(32x32)", 135, 75, 10, true, g);
		if(sizesSelection == 3) drawText("(64x64)", 135, 75, 10, true, g);
		
		for(int i = 0; i < sizes.length; i++){			
			drawText(sizes[i], 60 + (60 * i), 90, 12, true, g);
			if(sizesSelection == i && selection == 0) drawText(">", 50 + (60 * i), 90, 12, true, g);
			else if(sizesSelection == i && selection != 0) drawText(">", 50 + (60 * i), 90, 12, false, g);
		}
		
		drawText("Structure Name", 50, 120, 12, true, g);
		if(selection == 1) drawText(">", 40, 120, 12, true, g);
		
		if(name == null && selection == 1){
			drawText("Press enter to set your structure name.", 60, 135, 10, true, g);
			if(selection == 1) drawText(">", 50, 135, 10, true, g);
		}else if(name == null || name == ""){
			drawText("Unknown", 60, 135, 11, true, g);
		}else if(name != null){
			drawText(name, 60, 135, 11, true, g);
		}
		
		
		drawText("Start", 50, 300, 14, true, g);
		if(selection == 2) drawText(">", 40, 300, 14, true, g);
	}
	
	public void update(){
		input();
	}
	
	public void input(){
		if(input.esc.isPressed()) menu.setState(0);
		
		if(input.right.isPressed()){
			if(selection == 0){
				if(sizesSelection == (sizes.length - 1)) return;
				else sizesSelection++;
			}
		
		}
		
		if(input.left.isPressed()){
			if(selection == 0){
				if(sizesSelection == 0) return;
				else sizesSelection--;
			}
			
		}
		
		if(input.up.isPressed()){
			if(selection == 0) return;
			else selection--;
		}
		
		if(input.down.isPressed()){
			if(selection == 2) return;
			else selection++;
		}
		
		if(input.select.isPressed()){
			if(selection == 1) name = JOptionPane.showInputDialog("Enter structure name");
			if(selection == 2 && name != null) {
				System.out.println("Start tile editor");
				main.setState(1);
				main.setType(2);
				main.setStructSize(sizesSelection);
				main.setStructName(name);
			}
		}
	}

	public void drawText(String msg, int x, int y, int size, boolean bold, Graphics g){
		if(!bold) g.setFont(new Font("Arial", Font.PLAIN, size));
		if(bold) g.setFont(new Font("Arial", Font.BOLD, size));
		
		g.setColor(Color.BLACK);
		g.drawString(msg, x + 1, y + 1);
		g.setColor(Color.WHITE);
		g.drawString(msg, x, y);
	}

	public void reset() {
		selection = 0;
		name = null;
		sizesSelection = 0;
	}
}
