package com.egs.survivalsim.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import com.egs.survivalsim.util.InputHandler;

public class MainMenu {
	
	private InputHandler input;
	private Menu menu;
	
	private String[] options = {"New Game", "Structure Editor", "Instructions", "About"};
	private String[] subMenu1 = {"Simulation Mode", "Adventure Mode"};
	
	private int selection = 0;
	private int subMenu = -1;
	private int subSelection = 0;
	private int subPosition = 0;
	
	public MainMenu(InputHandler input, Menu menu) {
		this.input = input;
		this.menu = menu;
	}
	
	public void render(Graphics g){
		for(int i = 0; i < options.length; i++){
			drawText(options[i], 52, 50 + (12 * i), 12, true, g);
			if(selection == i) drawText(">", 42, 50 + (12 * i), 12, true, g);
		}
		
		if(subMenu != -1){
			for(int i = 0; i < getSubmenu().length; i++){
				drawText(getSubmenu()[i], 155, 50 + (12 * i) + (12 * subPosition), 12, true, g);
				if(subSelection == i) drawText(">", 145, 50 + (12 * i) + (12 * subPosition), 12, true, g);
			}
		}
		
		drawText("Selection: " + selection, 250, 250, 12, true, g);
		
		drawText("Use WASD and enter to use the menus.", 243, 390, 12, true, g);
		drawText("www.epicgamestudio.com", 289, 405, 10, true, g);

	}
	
	public void update(){
		if(input.up.isPressed()){
			if(selection == 0 && subMenu == -1)return;
			else if(subMenu == -1) selection--;
			
			if(subMenu != -1){
				if(subSelection == 0)return;
				else subSelection--;
			}
		}
		
		if(input.down.isPressed()){
			if(selection == (options.length - 1)) return;
			else if(subMenu == -1) selection++;
			
			if(subMenu != -1){
				if(subSelection == (getSubmenu().length - 1)) return;
				else subSelection++;
			}
		}
		
		if(input.right.isPressed()){
			if(subMenu != -1) return;
			if(selection == 0){
				subMenu = 0;
				subSelection = 0;
				subPosition = 0;
			}			
		}
		
		if(input.left.isPressed()){
			subMenu = -1;
			subSelection = 0;
		}
		
		
		if(input.select.isPressed() && subMenu != -1){
			if(selection == 0 && subSelection == 0) menu.setState(2);
			if(selection == 0 && subSelection == 1) menu.setState(1);
			selection = 0;
			subMenu = -1;
			subSelection = 0;
		}
		
		if(input.select.isPressed()){
			if(selection == 1) menu.setState(3);
			selection = 0;
			subMenu = -1;
			subSelection = 0;
		}
		
	}
	
	public void reset(){
		selection = 0;
		subMenu = -1;
		subSelection = 0;
		subPosition = 0;
	}
	
	public void drawText(String msg, int x, int y, int size, boolean bold, Graphics g){
		if(!bold) g.setFont(new Font("Arial", Font.PLAIN, size));
		if(bold) g.setFont(new Font("Arial", Font.BOLD, size));
		
		g.setColor(Color.BLACK);
		g.drawString(msg, x + 1, y + 1);
		g.setColor(Color.WHITE);
		g.drawString(msg, x, y);
	}
	
	private String[] getSubmenu(){
		if(subMenu == 0) return subMenu1;
		return null;
	}

}
