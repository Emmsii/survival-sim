package com.egs.survivalsim.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.util.Random;

import com.egs.survivalsim.MainComponent;
import com.egs.survivalsim.util.InputHandler;

public class Menu {

	private Random random = new Random();
	
	private MainMenu mainMenu;
	private InstMenu instMenu;
	private AboutMenu aboutMenu;
	private SimulationMenu simulationMenu;
	private AdventureMenu adventureMenu;
	private StructureMenu structMenu; 
	
	private int[][] background = new int[200][200];
	private double angle = 0.1;
	private int xa = 150;
	private int ya = 150;
	private int radius = 100;

	private int state = 0;

	public Menu(MainComponent main, InputHandler input){
		generateBackground();
		
		mainMenu = new MainMenu(input, this);
		instMenu = new InstMenu();
		aboutMenu = new AboutMenu();
		simulationMenu = new SimulationMenu(input, this, main);
		adventureMenu = new AdventureMenu(input, this, main);
		structMenu = new StructureMenu(input, this, main);
	}
	
	public void render(Graphics g){
		renderBackground(g);

		if(state == 0) mainMenu.render(g);//Render main menu
		if(state == 1) adventureMenu.render(g); //Render load
		if(state == 2) simulationMenu.render(g); //Render instructions
		if(state == 3) structMenu.render(g);
		if(state == 4) instMenu.render(g); //Render about
		if(state == 5) aboutMenu.render(g);
	}
	
	public void update(){
		if(state == 0) mainMenu.update(); //Update main menu
		if(state == 1) adventureMenu.update(); //Update load
		if(state == 2) simulationMenu.update(); //Update instructions
		if(state == 3) structMenu.update();
		if(state == 3) instMenu.update(); //Update about
		if(state == 4) aboutMenu.update();
	}
	
	public void generateBackground(){
		for(int y = 0; y < 200; y++){
			for(int x = 0; x < 200; x++){
				background[x][y] = 0;
				if(random.nextInt(10) == 0) background[x][y] = 1;
			}
		}
	}
	
	public void renderBackground(Graphics g){
		double rand = angle * 2.0 * Math.PI / 360;
		
		xa = (int) (Math.cos(rand) * radius);
		ya = (int) (Math.sin(rand) * radius);
		xa += 100;
		ya += 100;
		
		angle = angle + 0.225;
		
		for(int y = 0; y < 53; y++){
			for(int x = 0; x < 80; x++){
				int xPos = (x * 12);
				int yPos = (y * 12);
				
				xPos -= xa;
				yPos -= ya;
				
				g.setColor(new Color(55, 94, 25)); 
				g.fillRect(xPos, yPos - 12, 12, 12);
				if(background[x][y] == 0){
					g.setColor(new Color(19, 33, 9));
					g.setFont(new Font("Arial", Font.BOLD, 12));
					g.drawString("' '", xPos + 2, yPos + 5);
					g.setFont(new Font("Arial", Font.PLAIN, 12));
				}
				if(background[x][y] == 1){				
					g.setFont(new Font("Arial", Font.BOLD, 13));
					g.setColor(new Color(23, 43, 8)); 
					g.drawString("T", xPos + 2, yPos - 1);
					
					g.setColor(new Color(66, 43, 27)); 
					g.drawString("T", xPos + 2, yPos);
					g.setFont(new Font("Arial", Font.PLAIN, 12));
				}
			}
		}
	}
	
	public void resetMenus(){
		state = 0;
		mainMenu.reset();
		adventureMenu.reset();
		simulationMenu.reset();
		instMenu.reset();
		aboutMenu.reset();
		structMenu.reset();
	}
	
	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}
}
