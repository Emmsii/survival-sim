package com.egs.survivalsim.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import com.egs.survivalsim.MainComponent;
import com.egs.survivalsim.util.InputHandler;

public class AdventureMenu {

	private InputHandler input;
	private MainComponent main;
	private Menu menu;
	
	private int selection = 1;
	
	public AdventureMenu(InputHandler input, Menu menu, MainComponent main){
		this.input = input;
		this.menu = menu;
		this.main = main;
	}
	
	public void render(Graphics g){		
		drawText("Adventure Mode", 50, 50, 20, true, g);
		
		drawText("Start", 50, 300, 14, true, g);
		if(selection == 1) drawText(">", 40, 300, 14, true, g);
		
//		g.setColor(Color.BLACK);
//		g.drawString("This menu will have settings to start an adventure mode game.", 101, 151);
//		g.drawString("This menu does nothing, press esc to go back. ", 101, 162);
//		
//		g.setColor(Color.WHITE);
//		g.drawString("This menu will have settings to start an adventure mode game.", 100, 150);
//		g.drawString("This menu does nothing, press esc to go back.", 100, 161);
	}
	
	public void update(){
		input();
	}
	
	public void input(){
		if(input.esc.isPressed()) menu.setState(0);
		
		if(input.up.isPressed()){
			if(selection == 0) return;
			else selection--;
		}
		
		if(input.down.isPressed()){
			if(selection == 1) return;
			else selection++;
		}
		
		if(input.select.isPressed()){
			if(selection == 1){
				main.setState(1);
				main.setType(1);
			}
		}
	}
	
	public void drawText(String msg, int x, int y, int size, boolean bold, Graphics g){
		if(!bold) g.setFont(new Font("Arial", Font.PLAIN, size));
		if(bold) g.setFont(new Font("Arial", Font.BOLD, size));
		
		g.setColor(Color.BLACK);
		g.drawString(msg, x + 1, y + 1);
		g.setColor(Color.WHITE);
		g.drawString(msg, x, y);
	}

	public void reset() {
		selection = 0;
	}
}
