package com.egs.survivalsim.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import com.egs.survivalsim.MainComponent;
import com.egs.survivalsim.util.InputHandler;


public class SimulationMenu {

	private InputHandler input;
	private MainComponent main;
	private Menu menu;
	
	private String[] sizes = {"Small", "Medium", "Large", "Huge", "Epic"};
	private String[] times = {"Spring", "Summer", "Autumn", "Winter"};
	
	private int selection = 0;
	private int sizesSelection = 0;
	private int timesSelection = 0;
	
	public SimulationMenu(InputHandler input, Menu menu, MainComponent main){
		this.input = input;
		this.menu = menu;
		this.main = main;
	}
	
	public void render(Graphics g){
		drawText("Simulation Mode", 30, 40, 20, true, g);
		drawText("Choose your settings for a simulation mode game. Note this mode is unfinished.", 32, 52, 11, true, g);

		drawText("World Size", 50, 75, 12, true, g);
		
		if(selection == 0) drawText(">", 40, 75, 12, true, g);
		
		if(sizesSelection == 0) drawText("(128x128)", 116, 75, 10, true, g);
		if(sizesSelection == 1) drawText("(256x256)", 116, 75, 10, true, g);
		if(sizesSelection == 2) drawText("(512x512)", 116, 75, 10, true, g);
		if(sizesSelection == 3) drawText("(1024x1024)", 116, 75, 10, true, g);
		if(sizesSelection == 4) drawText("(2048x2048)", 116, 75, 10, true, g);
		
		for(int i = 0; i < sizes.length; i++){			
			drawText(sizes[i], 60 + (60 * i), 90, 12, true, g);
			if(sizesSelection == i && selection == 0) drawText(">", 50 + (60 * i), 90, 12, true, g);
			else if(sizesSelection == i && selection != 0) drawText(">", 50 + (60 * i), 90, 12, false, g);
		}
		
		drawText("Start Time", 50, 120, 12, true, g);
		if(selection == 1) drawText(">", 40, 120, 12, true, g);

		for(int i = 0; i < times.length; i++){
			drawText(times[i], 60 + (75 * i), 135, 12, true, g);
			if(timesSelection == i && selection == 1) drawText(">", 50 + (75 * i), 135, 12, true, g);
			else if(timesSelection == i && selection != 1) drawText(">", 50 + (75 * i), 135, 12, false, g);
		}
		
		drawText("Start", 50, 300, 14, true, g);
		if(selection == 2) drawText(">", 40, 300, 14, true, g);

	}
	
	public void update(){
		input();
	}
	
	public void input(){
		if(input.esc.isPressed()) menu.setState(0);
		
		if(input.right.isPressed()){
			if(selection == 0){
				if(sizesSelection == (sizes.length - 1)) return;
				else sizesSelection++;
			}
			
			if(selection == 1){
				if(timesSelection == (times.length - 1)) return;
				else timesSelection++;
			}
		}
		
		if(input.left.isPressed()){
			if(selection == 0){
				if(sizesSelection == 0) return;
				else sizesSelection--;
			}
			
			if(selection == 1){
				if(timesSelection == 0) return;
				else timesSelection--;
			}
		}
		
		if(input.up.isPressed()){
			if(selection == 0) return;
			else selection--;
		}
		
		if(input.down.isPressed()){
			if(selection == 2) return;
			else selection++;
		}
		
		if(input.select.isPressed()){
			if(selection == 2){
				main.setStartSize(sizesSelection);
				System.out.println("START SIZE: " + sizesSelection);
				main.setStartTime(timesSelection);
				main.setState(1);
				main.setType(0);
			}
		}
	}
	
	public void drawText(String msg, int x, int y, int size, boolean bold, Graphics g){
		if(!bold) g.setFont(new Font("Arial", Font.PLAIN, size));
		if(bold) g.setFont(new Font("Arial", Font.BOLD, size));
		
		g.setColor(Color.BLACK);
		g.drawString(msg, x + 1, y + 1);
		g.setColor(Color.WHITE);
		g.drawString(msg, x, y);
	}

	public void reset() {
		selection = 0;
		sizesSelection = 0;
		timesSelection = 0;
	}
}
