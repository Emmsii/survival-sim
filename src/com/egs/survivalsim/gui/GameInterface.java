package com.egs.survivalsim.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;

import com.egs.survivalsim.MainComponent;
import com.egs.survivalsim.game.GameWindow;
import com.egs.survivalsim.game.Time;

public class GameInterface {

	private MainComponent main;
	private GameWindow window;
	private Time time;
	
	private List<String> console = new ArrayList<String>();
	
	private int type;
	
	public GameInterface(MainComponent main, GameWindow window, Time time, int type){
		this.main = main;
		this.window = window;
		this.time = time;
		this.type = type;
	}
	
	public void render(Graphics g){
		g.setColor(Color.WHITE);
		g.setFont(new Font("Arial", Font.PLAIN, 10));
		
		
		if(type != 2){
			g.drawString(time.getTime(), 510, 180);
			g.drawString(time.getDate(), 510, 190);
	//		if(time.getSeason() == 0) g.drawString("Winter", 510, 200);
	//		if(time.getSeason() == 1) g.drawString("Spring", 510, 200);
	//		if(time.getSeason() == 2) g.drawString("Summer", 510, 200);
	//		if(time.getSeason() == 3) g.drawString("Autumn", 510, 200);
		}
		
		g.drawString("|", 4, 8);
		g.drawString("fps: " + main.getFps(), 9, 9);
		g.drawString("ups: " + main.getUps(), 49, 9);
		g.drawString("u: " + window.getUpdateTime() + "ms", 86, 9);
		g.drawString("r: " + window.getRenderTime() + "ms", 143, 9);
		g.drawString("|", 195, 8);
		g.drawString("steps: " + window.getSteps(), 202, 9);
		
		g.drawLine(510, 205, 715, 205);
		g.drawLine(510, 205, 510, 405);
		g.drawLine(715, 405, 715, 205);
		g.drawLine(510, 405, 715, 405);
				
		for(int i = 0; i < console.size(); i++){
			String msg = console.get(i);
			
			if(console.size() <= 16) g.drawString("> " + msg, 513, 215 + (i * 12));
			else if(i >= console.size() - 16)g.drawString("> " + msg, 513, 215 + ((i - (console.size() - 16)) * 12));
		}
	}
	
	public void update(long steps){
		time.update(steps);
	}
	
	public void addConsole(String msg){
		console.add(msg);
	}
}
